# -*- coding: utf-8 -*-

"""
Конфигурационный файл приложения интернет-магазина Oscar
http://tangentlabs.github.io/django-oscar/

Проект документации доступен по адресу
https://bitbucket.org/soshnikov/oscar-setup
"""

import os
from oscar import get_core_apps
from oscar.defaults import *
from oscar import OSCAR_MAIN_TEMPLATE_DIR

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
PROJECT_DIR = BASE_DIR
SECRET_KEY = 't29slllyu=*66^cug(va!+hz5i79%8rcp&axw8sa4$x!%xyvbw'
DEBUG = True
TEMPLATE_DEBUG = True
ALLOWED_HOSTS = ['store.example.com', '.store.example.com']

"""
Это настройки модуля django-compressor, предназначенного для склейки и
сжатия JS и CSS
Сайт проекта: http://django-compressor.readthedocs.org/en/latest/
Небольшая заметка на русском: http://vorushin.ru/blog/33-django-compressor/ 
и http://michael-borisov.com/2015/07/17/django-compressor/
"""
COMPRESS_ROOT = BASE_DIR  #Управляет абсолютным путем к файлам, связаным со статическими файлами и будущими (сжатыми) файлами
"""
COMPRESS_ROOT по умолчанию COMPRESS_STORAGE
TODO: разобраться подробнее с компрессором и настройками
"""

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',
    'django.contrib.sites',
    'south', #south в последних версиях лучше не использовать (предназначен для миграций)
    'compressor',
    'extore', #наше приложение для подключение к django-oscar
] + get_core_apps(['extore.promotions'])#проброс приложения  в app django-oscar

SITE_ID = 1
#Начиная с Django 1.10 изменился принцип добавления MIDDLEWARE
'''
Пример разницы настроек для версии Django до 1.10 и после
if DJANGO_VERSION < (1, 10):
    MIDDLEWARE_CLASSES = [
        'django.middleware.common.CommonMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',

        'oscar.apps.basket.middleware.BasketMiddleware',
    ]
else:
    MIDDLEWARE = [
        'django.middleware.common.CommonMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',

        'oscar.apps.basket.middleware.BasketMiddleware',
    ]
'''

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'oscar.apps.basket.middleware.BasketMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)
AUTHENTICATION_BACKENDS = (
    'oscar.apps.customer.auth_backends.Emailbackend',
    'django.contrib.auth.backends.ModelBackend',
)
ROOT_URLCONF = 'ostore.urls'
WSGI_APPLICATION = 'ostore.wsgi.application'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ostore',
        'USER': 'developer',
        'PASSWORD': 'developer',
        'HOST': 'localhost',
        'PORT': '',
    }
}
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True
"""
Если параметр LANGUAGES не указан, в левом верхнем углу страницы будет 
отображаться выпадающий список со всеми поддерживаемыми языками перевода
интерфейса магазина.
Если указать единственный язык - выпадающий список не будет отображаться вообще.
Если указать несколько языков, то список будет содержать только их.
"""
LANGUAGES = (
    ('ru', 'Russian'),
)
LANGUAGE_CODE = 'ru'

STATIC_URL = '/static/'

location = lambda x: os.path.join(
    os.path.dirname(os.path.realpath(__file__)), x)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.request",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "oscar.apps.search.context_processors.search_form",
    "oscar.apps.promotions.context_processors.promotions",
    "oscar.apps.checkout.context_processors.checkout",
    "oscar.apps.customer.notifications.context_processors.notifications",
    "oscar.core.context_processors.metadata",
)
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)
TEMPLATE_DIRS = (
    location('templates'),
    OSCAR_MAIN_TEMPLATE_DIR,
)
MEDIA_ROOT = location("public/media")
MEDIA_URL = '/media/'
# HAYSTACK_CONNECTIONS отвечает за поиск по сайту
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
    },
}
SERVER_EMAIL = 'admin@example.com'
#Настройки магазина
OSCAR_SHOP_NAME = u'Магазинчик'
OSCAR_SHOP_TAGLINE = u'лучшее место для покупок в сети'
OSCAR_DEFAULT_CURRENCY = 'RUR'
OSCAR_CURRENCY_LOCALE = 'ru_RU'


#OSCAR_HOMEPAGE
"""
По умолчанию: reverse_lazy('promotions:home')
URL домашней страницы вашего сайта. Это значение используется для домашней ссылки в навигации и перенаправления страницы 
после выхода из системы. Полезно, если вы используете другое приложение (не promotions), для своей главной страницей.
"""

#OSCAR_RECENTLY_VIEWED_COOKIE_LIFETIME
"""
По умолчанию: 604800 (1 неделя в секундах). Время жизини для cookie в секундах.
"""

"""
OSCAR_REQUIRED_ADDRESS_FIELDS определяет обязательные поля при оформлении
заказа.
С точки зрения покупателя, меня бесит необходимость заполнять поля,
которые не нужны. Зачем, например, обязывать заполнять адрес доставки,
если я хочу забрать покупку самовывозом?
Поэтому оставляйте только действительно необходимое.
"""
OSCAR_REQUIRED_ADDRESS_FIELDS = (
    'first_name', 
#    'last_name', 
#    'line1', 
#    'line4', 
#    'postcode', #  TODO: все равно требует ввести индекс, хоть и не помечает, как обязательное
#    'country',
    )
OSCAR_PRODUCTS_PER_PAGE = 20  # количество товаров на странице
OSCAR_ALLOW_ANON_CHECKOUT = False  # разрешить покупки без регистрации
OSCAR_ALLOW_ANON_REVIEWS = True  # разрешить анонимные отзывы о товаре
OSCAR_MODERATE_REVIEWS = False  # проверка отзывов перед публикацией на сайте
"""
Следующий параметр разрешает немедленную отсылку уведомлений о поступившем
товаре покупателям, которые просили их уведомить. Может создавать значительную
нагрузку на сервер при больших объемах рассылки. не рекомендуется использовать.
Подробнее смотрите в документации.
"""
OSCAR_EAGER_ALERTS = True
"""
Следующий параметр определяет, надо ли отсылать покупателям уведомление о
регистрации в магазине. Стоит отметить, что подтверждения регистрации не
требуется, так что отсылается только информационное уведомление.
"""
OSCAR_SEND_REGISTRATION_EMAIL = True
OSCAR_FROM_EMAIL = 'webmaster@example.com'
OSCAR_RECENTLY_VIEWED_PRODUCTS = 20  # количество запоминаемых недавно просмотренных товаров
OSCAR_MAX_BASKET_QUANTITY_THRESHOLD = None  # количество товаров, которые можно добавить в корзину

#OSCAR_MAIN_TEMPLATE_DIR = '/home/qwe/projects/python/oscar/local/lib/python2.7/site-packages/oscar/templates/oscar'

"""
Настройка статусов заказов.
Для каждого статуса можно указать статусы, на которые можно изменить текущий.
"""
OSCAR_INITIAL_ORDER_STATUS = u'Новый'
OSCAR_INITIAL_LINE_STATUS = u'Новый'
OSCAR_LINE_STATUS_PIPELINE = {
    u'Новый': (u'Подготовлен', u'Отменен',),
    u'Подготовлен': (u'Отгружен', u'Отменен',),
    u'Отгружен': (),
    u'Отменен': (),
}