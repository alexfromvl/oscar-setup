# -*- coding: utf-8 -*-

"""
Основной модуль приложения

Created: 05.03.14 1:31
Author: Ivan Soshnikov, e-mail: ivan@wtp.su
"""

from oscar.apps.promotions.app import PromotionsApplication as CorePromotionApplication

from extore.promotions.views import HomeView


class PromotionsApplication(CorePromotionApplication):
    """
    Переопределение вьюхи базового магазина на измененную
    """

    home_view = HomeView

application = PromotionsApplication()